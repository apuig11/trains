package com.apuig.trains.core.test.service.impl;

import com.apuig.trains.core.exception.NoSuchRouteException;
import com.apuig.trains.core.exception.NoSuchStopException;
import com.apuig.trains.core.model.Route;
import com.apuig.trains.core.service.RailroadRouteService;
import com.apuig.trains.core.service.impl.ShortestRouteServiceImpl;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class ShortestRouteServiceImplTest extends AbstractRouteServiceTest {

    private final RailroadRouteService shortestRouteService = ShortestRouteServiceImpl.getInstance();

    @Override
    @Test
    void testGetRoute() {

        Route route = this.shortestRouteService.getRoute(this.railroadGraph, "A-E-D");

        assertEquals(route.getTotalDistance(),22);
    }

    @Override
    @Test
    void testGetInvalidRoute() {

        assertThrows(NoSuchRouteException.class,
                () -> this.shortestRouteService.getRoute(this.railroadGraph, "A-E-F"));
    }

    @Override
    @Test
    void testGetInvalidStop() {

        assertThrows(NoSuchStopException.class,
                () -> this.shortestRouteService.getRoute(this.railroadGraph, "A-K"));
    }

}