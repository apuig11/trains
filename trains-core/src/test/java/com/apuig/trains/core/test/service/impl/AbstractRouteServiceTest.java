package com.apuig.trains.core.test.service.impl;

import com.apuig.trains.core.model.RailroadGraph;
import com.apuig.trains.core.model.RailroadStop;
import com.apuig.trains.core.model.Track;
import org.junit.jupiter.api.Test;

abstract class AbstractRouteServiceTest {
    
    final RailroadGraph railroadGraph = this.initGraph();

    @Test
    abstract void testGetRoute();

    @Test
    abstract void testGetInvalidRoute();

    @Test
    abstract void testGetInvalidStop();
    
    private RailroadGraph initGraph() {
        
        RailroadStop A = new RailroadStop("A");

        RailroadStop B = new RailroadStop("B");

        RailroadStop C = new RailroadStop("C");

        RailroadStop D = new RailroadStop("D");

        RailroadStop E = new RailroadStop("E");

        RailroadStop F = new RailroadStop("F");

        Track AB = new Track(B, 5);
        Track AD = new Track(D, 5);
        Track AE = new Track(E, 7);

        A.addTrack(AB);
        A.addTrack(AD);
        A.addTrack(AE);

        Track BC = new Track(C, 4);

        B.addTrack(BC);

        Track CD = new Track(D, 8);
        Track CE = new Track(E, 2);

        C.addTrack(CD);
        C.addTrack(CE);

        Track DC = new Track(C, 8);
        Track DE = new Track(E, 6);

        D.addTrack(DC);
        D.addTrack(DE);

        Track EB = new Track(B, 3);

        E.addTrack(EB);

        Track FA = new Track(A, 3);

        F.addTrack(FA);

        RailroadGraph railroadGraph = new RailroadGraph();

        railroadGraph.addTownStop(A);
        railroadGraph.addTownStop(B);
        railroadGraph.addTownStop(C);
        railroadGraph.addTownStop(D);
        railroadGraph.addTownStop(E);
        railroadGraph.addTownStop(F);

        return railroadGraph;
    }
    
}
