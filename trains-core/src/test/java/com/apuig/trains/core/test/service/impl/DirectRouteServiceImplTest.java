package com.apuig.trains.core.test.service.impl;

import com.apuig.trains.core.exception.NoSuchRouteException;
import com.apuig.trains.core.exception.NoSuchStopException;
import com.apuig.trains.core.model.Route;
import com.apuig.trains.core.service.RailroadRouteService;
import com.apuig.trains.core.service.impl.DirectRouteServiceImpl;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class DirectRouteServiceImplTest extends AbstractRouteServiceTest {

    private final RailroadRouteService directRouteService = DirectRouteServiceImpl.getInstance();

    @Override
    @Test
    void testGetRoute() {

        Route route = this.directRouteService.getRoute(this.railroadGraph, "A-B-C");

        assertEquals(route.getTotalDistance(),9);
    }

    @Override
    @Test
    void testGetInvalidRoute() {

        assertThrows(NoSuchRouteException.class,
                () -> this.directRouteService.getRoute(this.railroadGraph, "A-E-D"));
    }

    @Override
    @Test
    void testGetInvalidStop() {

        assertThrows(NoSuchStopException.class,
                () -> this.directRouteService.getRoute(this.railroadGraph, "A-K"));
    }
}
