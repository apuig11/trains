package com.apuig.trains.core.test.factory;

import com.apuig.trains.core.exception.RouteAlreadyExistingException;
import com.apuig.trains.core.factory.RailroadFactory;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class RailroadFactoryTest {

    @Test
    void getValidInstance() {

        assertNotNull(RailroadFactory.getInstance("AB1,BC3,BA5"));
    }

    @Test
    void getIllegalArgumentForInput() {

        assertThrows(IllegalArgumentException.class, () -> RailroadFactory.getInstance("AB4, A4C"));
    }

    @Test
    void getRouteAlreadyExistingForInput() {


        assertThrows(RouteAlreadyExistingException.class, () -> RailroadFactory.getInstance("AB3,BC4,AB4"));
    }

}
