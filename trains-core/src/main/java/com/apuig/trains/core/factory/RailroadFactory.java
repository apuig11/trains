package com.apuig.trains.core.factory;

import com.apuig.trains.core.exception.RouteAlreadyExistingException;
import com.apuig.trains.core.model.RailroadGraph;
import com.apuig.trains.core.model.RailroadStop;
import com.apuig.trains.core.model.Track;
import com.apuig.trains.core.util.RouteUtil;

import java.util.stream.Stream;

/**
 * Factory class for railroad graph {@link RailroadGraph} instances.
 *
 * @author Alejandro Puig Escobar
 */
public class RailroadFactory {

    /**
     * Creates an instance of a railroad graph {@link RailroadGraph} given user input.
     *
     * @param userInput graph string.
     * @return {@link RailroadGraph} instance.
     * @throws IllegalArgumentException if the input has an invalid format.
     * @throws RouteAlreadyExistingException if there is any duplicated node connection.
     */
    public static RailroadGraph getInstance(String userInput) throws IllegalArgumentException, RouteAlreadyExistingException {

        String[] railroadPaths = RouteUtil.RAILROAD_MAP_DELIMITER.split(userInput);

        final RailroadGraph railroadGraph = new RailroadGraph();

        for (String railroadPath : railroadPaths) {

            if (RouteUtil.RAILROAD_REGEX.matcher(railroadPath).matches()) {

                String[] stopNames = Stream.of(RouteUtil.RAILROAD_TRACK_DISTANCE_DELIMITER.split(railroadPath))
                        .map(union -> union.split("")).findFirst().get();

                long trackDistance = Stream.of(RouteUtil.STOP_NAME_PATTERN.split(railroadPath))
                                    .filter(distance -> !distance.isEmpty())
                                    .map(Long::valueOf)
                                    .findFirst().get();

                RailroadStop origin = findOrCreateStop(railroadGraph, stopNames[0]),
                             destination = findOrCreateStop(railroadGraph, stopNames[1]);

                if (routeAlreadyExists(origin, destination)) {

                    throw new RouteAlreadyExistingException();

                } else {

                    Track connectingTrack = new Track(destination, trackDistance);

                    origin.addTrack(connectingTrack);

                    //if origin or destination are already contained they will not be added into the set
                    railroadGraph.addTownStop(origin);
                    railroadGraph.addTownStop(destination);
                }

            } else throw new IllegalArgumentException();

        }

        return railroadGraph;
    }

    /**
     * Performs a search by stop name in a given graph, creates a new stop if not found.
     *
     * @param railroadGraph the graph in which we will perform the search.
     * @param stopName name of the stop.
     * @return {@link RailroadStop}.
     */
    private static RailroadStop findOrCreateStop(RailroadGraph railroadGraph, String stopName) {

        return railroadGraph.getRailroadStops().stream()
                .filter(railroadStop -> railroadStop.getName().equals(stopName))
                .findFirst()
                .orElse(new RailroadStop(stopName));
    }

    /**
     * Checks whether a stop has already a destination or not.
     *
     * @param origin stop.
     * @param destination stop.
     * @return whether the route already exists or not.
     */
    private static boolean routeAlreadyExists(RailroadStop origin, RailroadStop destination) {

        return origin.getTracks().stream().anyMatch(track -> track.getDestination().equals(destination));
    }
}
