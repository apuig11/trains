package com.apuig.trains.core.service;

import com.apuig.trains.core.exception.NoSuchRouteException;
import com.apuig.trains.core.model.RailroadGraph;
import com.apuig.trains.core.model.RailroadStop;
import com.apuig.trains.core.model.Route;
import com.apuig.trains.core.model.ShortestRoute;

/**
 * Defines the API that shortest route service implementors will have.
 *
 * @author Alejandro Puig Escobar
 */
public interface ShortestRouteService extends RailroadRouteService {

    /**
     * Gets the shortest route of a given graph {@link RailroadGraph} from an origin to a destination.
     *
     * @param railroadGraph the graph in question.
     * @param origin the origin node.
     * @param destination the destination node.
     * @return the shortest route {@link ShortestRoute} calculated from the origin to the destination.
     * @throws {@link NoSuchRouteException}
     */
    Route getShortestRoute(RailroadGraph railroadGraph, RailroadStop origin, RailroadStop destination)
            throws NoSuchRouteException;

}
