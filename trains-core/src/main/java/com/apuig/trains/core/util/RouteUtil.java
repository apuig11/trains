package com.apuig.trains.core.util;


import com.apuig.trains.core.exception.NoSuchRouteException;
import com.apuig.trains.core.exception.NoSuchStopException;
import com.apuig.trains.core.model.RailroadGraph;
import com.apuig.trains.core.model.RailroadStop;
import com.apuig.trains.core.model.Track;

import java.util.regex.Pattern;

public class RouteUtil {

    public static final Pattern STOP_NAME_PATTERN = Pattern.compile("([A-Z])");

    //assuming the user will not introduce a distance of 0
    public static final Pattern RAILROAD_REGEX = Pattern.compile("([A-Z])([A-Z])([1-9])+(\\d)*");

    public static final Pattern RAILROAD_TRACK_DISTANCE_DELIMITER = Pattern.compile("([1-9])+(\\d)*");

    public static final Pattern RAILROAD_MAP_DELIMITER = Pattern.compile(",");

    private static final Pattern STOP_NAMES_DELIMITER = Pattern.compile("-");

    /**
     * Finds a railroad stop by name given a graph containing all railroad stops.
     *
     * @param railroadGraph graph of all railroad stops.
     * @param stopName name of the stop to find.
     * @return {@link RailroadStop} if found.
     * @throws NoSuchStopException if not found.
     */
    @SuppressWarnings("WeakerAccess")
    public static RailroadStop findRailroadStop(RailroadGraph railroadGraph, String stopName) throws NoSuchStopException {

        return railroadGraph.getRailroadStops().stream()
                .filter(stop -> stop.getName().equals(stopName))
                .findFirst()
                .orElseThrow(NoSuchStopException::new);

    }

    /**
     * Returns an array of railroad stops each of them found by stop name.
     *
     * @param railroadGraph graph of all railroad stops.
     * @param stopNames name of the stops to find.
     * @return {@link RailroadStop} array if stops are found.
     * @throws IllegalArgumentException if there is any problem validating stop names input.
     */
    public static RailroadStop[] getRailroadStops(RailroadGraph railroadGraph, String stopNames) throws IllegalArgumentException {

        String[] stopNamesArr = RouteUtil.STOP_NAMES_DELIMITER.split(stopNames);

        if (stopNamesArr.length < 2) {

            throw new IllegalArgumentException();

        } else {

            RailroadStop[] railroadStops = new RailroadStop[stopNamesArr.length];

            for (int i = 0; i < stopNamesArr.length; i++) {

                String stopName = stopNamesArr[i];

                if (RouteUtil.STOP_NAME_PATTERN.matcher(stopName).matches()) {

                    railroadStops[i] = RouteUtil.findRailroadStop(railroadGraph, stopName);

                } else {

                    throw new IllegalArgumentException();

                }

            }

            return railroadStops;

        }

    }

    /**
     * Returns the distance between two stops directly connected by a track {@link Track}.
     *
     * @param origin stop to search from.
     * @param target stop to which to get the distance.
     * @return the distance between the two stops.
     * @throws NoSuchStopException if they are not directly connected.
     */
    public static long getDirectStopDistance(RailroadStop origin, RailroadStop target) throws NoSuchStopException {
        return origin.getTracks().stream()
                .filter(track -> track.getDestination().equals(target))
                .map(Track::getDistance)
                .findFirst()
                .orElseThrow(NoSuchRouteException::new);
    }

    /**
     * Returns whether the origin stop has the target stop as direct stop.
     *
     * @param origin stop.
     * @param target stop.
     * @return if they are directly connected.
     */
    public static boolean isDirectStop(RailroadStop origin, RailroadStop target) throws NoSuchStopException {
        return origin.getTracks().stream()
                .anyMatch(track -> track.getDestination().equals(target));
    }

    /**
     * Resets stops costs for SPF algorithm.
     *
     * @param railroadGraph the graph whose stops have to be reset.
     */
    public static void resetCosts(RailroadGraph railroadGraph) {
        railroadGraph.getRailroadStops().forEach(stop -> { stop.setCost(Long.MAX_VALUE); stop.setPreviousStop(null); });
    }


}
