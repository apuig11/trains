package com.apuig.trains.core.service.impl;

import com.apuig.trains.core.exception.NoSuchRouteException;
import com.apuig.trains.core.model.DirectRoute;
import com.apuig.trains.core.model.RailroadGraph;
import com.apuig.trains.core.model.RailroadStop;
import com.apuig.trains.core.service.RailroadRouteService;
import com.apuig.trains.core.util.RouteUtil;

import java.util.List;

/**
 * Direct route service finder implementation for {@link RailroadRouteService}.
 *
 * @author Alejandro Puig Escobar
 */
public class DirectRouteServiceImpl implements RailroadRouteService {

    private static DirectRouteServiceImpl routeService;

    public static DirectRouteServiceImpl getInstance() {

        if (routeService == null) {

            routeService = new DirectRouteServiceImpl();

        }

        return routeService;
    }

    private DirectRouteServiceImpl() {
    }


    /**
     * Gets the direct route for the given stop names.
     *
     * @param railroadGraph the graph in question.
     * @param stopNames stops that make up a route.
     * @return direct route between the stops.
     */
    @Override
    public DirectRoute getRoute(RailroadGraph railroadGraph, String stopNames) {

        RailroadStop[] railroadStops = RouteUtil.getRailroadStops(railroadGraph, stopNames);

        DirectRoute directRoute = new DirectRoute();

        List<RailroadStop> directRouteStops = directRoute.getStops();

        for (int i = 0; i < railroadStops.length - 1; i++) {

            RailroadStop origin = railroadStops[i], destination = railroadStops[i + 1];

            if (i == 0) {
                directRouteStops.add(origin);
            }

            if (RouteUtil.isDirectStop(origin, destination)) {

                destination.setPreviousStop(origin);
                directRouteStops.add(destination);

            } else {

                throw new NoSuchRouteException();

            }
        }

        return directRoute;
    }
}
