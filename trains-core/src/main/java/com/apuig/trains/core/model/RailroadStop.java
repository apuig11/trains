package com.apuig.trains.core.model;

import java.util.ArrayList;
import java.util.List;

/**
 * {@code com.apuig.trains.core.model.RailroadStop} is the representation of a node in a graph, where
 * the graph is of type {@link RailroadGraph}.
 *
 * It has a list of {@link Track} as edges.
 *
 * Is also prepared to run on shortest path finder algorithms, hence keeping a reference
 * to previous path node and having an associated cost.
 *
 * @author Alejandro Puig Escobar
 */
public class RailroadStop {

    private String name;

    private long cost = Long.MAX_VALUE;

    private List<Track> tracks = new ArrayList<>();

    private RailroadStop previousStop;

    public RailroadStop(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public long getCost() {
        return cost;
    }

    public void setCost(long cost) {
        this.cost = cost;
    }

    public List<Track> getTracks() {
        return tracks;
    }

    public void addTrack(Track track) {
        this.tracks.add(track);
    }

    public RailroadStop getPreviousStop() {
        return previousStop;
    }

    public void setPreviousStop(RailroadStop previousStop) {
        this.previousStop = previousStop;
    }

    @Override
    public String toString() {
        final StringBuilder strBuilder = new StringBuilder();

        this.tracks.stream()
                .forEach(track -> strBuilder.append("Railroad stop " + this.getName() + " to "
                        + track.getDestination().getName() + " with distance " + track.getDistance() + "."
                        + System.lineSeparator()));

        return strBuilder.toString();
    }
}
