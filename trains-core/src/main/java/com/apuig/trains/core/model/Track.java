package com.apuig.trains.core.model;

/**
 * {@code com.apuig.trains.core.model.Track} is the representation of an edge connecting nodes in a
 * graph, where the nodes and the graph are of type {@link RailroadStop} and {@link RailroadGraph}
 * respectively.
 *
 * @author Alejandro Puig Escobar
 */
public class Track {

    private RailroadStop destination;

    private long distance;

    public Track(RailroadStop destination, long distance) {
        this.destination = destination;
        this.distance = distance;
    }

    public RailroadStop getDestination() {
        return destination;
    }

    public long getDistance() {
        return distance;
    }
}
