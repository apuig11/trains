package com.apuig.trains.core.model;

import com.apuig.trains.core.util.RouteUtil;

import java.util.Stack;

/**
 * This class represents a route made up by several rail stops {@link RailroadStop}.
 *
 * @author Alejandro Puig Escobar
 */
public class ShortestRoute extends Route {

    private Stack<RailroadStop> stops = new Stack<>();

    public ShortestRoute() {
    }

    @Override
    public Stack<RailroadStop> getStops() {
        return this.stops;
    }

    @Override
    public long getTotalDistance() {

        RailroadStop[] stopsArr = (RailroadStop[]) this.stops.toArray(new RailroadStop[]{});

        long cost = 0;

        for (int i = stopsArr.length - 1; i > 0; i--) {

            RailroadStop origin = stopsArr[i], destination = stopsArr[i - 1];

            cost += RouteUtil.getDirectStopDistance(origin, destination);

        }

        return cost;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();

        RailroadStop[] stopsArr = this.stops.toArray(new RailroadStop[]{});

        stringBuilder.append("Shortest route is as follows:");
        stringBuilder.append(System.lineSeparator());

        long cost = 0;

        for (int i = stopsArr.length - 1; i > 0; i--) {

            RailroadStop origin = stopsArr[i], destination = stopsArr[i - 1];

            cost += RouteUtil.getDirectStopDistance(origin, destination);

            this.setStopInfo(origin, destination, cost, stringBuilder);

        }

        return stringBuilder.toString();
    }
}
