package com.apuig.trains.core.service.impl;

import com.apuig.trains.core.exception.NoSuchRouteException;
import com.apuig.trains.core.model.RailroadGraph;
import com.apuig.trains.core.model.RailroadStop;
import com.apuig.trains.core.model.ShortestRoute;
import com.apuig.trains.core.model.Track;
import com.apuig.trains.core.service.ShortestRouteService;
import com.apuig.trains.core.util.RouteUtil;

import java.util.*;

/**
 * Shortest route service finder implementation for {@link ShortestRouteService} based on Dijkstra's shortest path finder
 * algorithm.
 *
 * @author Alejandro Puig Escobar
 */
public class ShortestRouteServiceImpl implements ShortestRouteService {

    private static ShortestRouteServiceImpl routeService;

    public static ShortestRouteServiceImpl getInstance() {

        if (routeService == null) {

            routeService = new ShortestRouteServiceImpl();

        }

        return routeService;
    }

    private ShortestRouteServiceImpl() {
    }

    /**
     * This service method gets the shortest route of a given graph {@link RailroadGraph} using
     * Dijkstra's SPF algorithm.
     *
     * @param railroadGraph the graph containing all rail stops.
     * @param origin the rail stop from where we are running the search.
     * @param destination the target rail stop we need to find with the least possible cost
     * @return a route {@link ShortestRoute} containing a sequence of rail stops starting from the
     *         origin and ending with the destination stop.
     * @throws NoSuchRouteException when no route is found from the origin to the destination.
     */
    @Override
    public final ShortestRoute getShortestRoute(RailroadGraph railroadGraph, RailroadStop origin, RailroadStop destination)
            throws  NoSuchRouteException {

        Set<RailroadStop> uncheckedStops = new HashSet<>();
        Collections.addAll(uncheckedStops, railroadGraph.getRailroadStops().toArray(new RailroadStop[]{}));

        Set<RailroadStop> checkedStops = new HashSet<>();

        origin.setCost(0);

        while (!uncheckedStops.isEmpty()) {

            RailroadStop fromRailroadStop = this.getCheapestTrip(uncheckedStops);

            if (fromRailroadStop.getCost() == Long.MAX_VALUE) {

                break;

            }

            uncheckedStops.remove(fromRailroadStop);

            //if the condition is true we have already found the shortest path
            if (fromRailroadStop.equals(destination)) {

                return this.traverseStops(fromRailroadStop);

            }

            else {

                for (Track withTrack : fromRailroadStop.getTracks()) {

                    RailroadStop toRailroadStop = withTrack.getDestination();

                    if (!checkedStops.contains(toRailroadStop)) {

                        this.updateTripCost(fromRailroadStop, toRailroadStop, withTrack.getDistance());

                    }
                }
            }

            checkedStops.add(fromRailroadStop);

        }

        RouteUtil.resetCosts(railroadGraph);
        throw new NoSuchRouteException();
    }

    /**
     * Gets the shortest route between two or more stops by using Dijkstra's SPF
     * algorithm in a loop.
     *
     * @param railroadGraph the graph in question.
     * @param stopNames stops to calculate.
     * @return the shortest route between the given stop names.
     * @throws NoSuchRouteException when there is no route.
     */
    @Override
    public ShortestRoute getRoute(RailroadGraph railroadGraph, String stopNames) throws NoSuchRouteException {

        RailroadStop[] railroadStops = RouteUtil.getRailroadStops(railroadGraph, stopNames);

        ShortestRoute shortestRoute = null;

        for (int i = 0; i < railroadStops.length - 1; i++) {

            RailroadStop origin = railroadStops[i], destination = railroadStops[i + 1];

            shortestRoute = shortestRoute == null ? this.getShortestRoute(railroadGraph, origin, destination) :
                    this.mergeRoutes(shortestRoute, this.getShortestRoute(railroadGraph, origin, destination));

            RouteUtil.resetCosts(railroadGraph);
        }

        return shortestRoute;

    }

    /**
     * Merges calculated routes stacks to make up the full route between more than two stops.
     *
     * @param routes routes to merge
     * @return merged route {@link ShortestRoute}
     */
    private ShortestRoute mergeRoutes(ShortestRoute... routes) {

        ShortestRoute mergedRoute = null;

        for (int i = 0; i < routes.length - 1; i++) {

            ShortestRoute fromRoute = routes[i];
            ShortestRoute toMergeRoute = routes[i + 1];

            //assuming stack is not empty and that last route[i] last stop is = to route[i +1] first stop
            toMergeRoute.getStops().pop();

            fromRoute.getStops().forEach(stop -> toMergeRoute.getStops().push(stop));

            mergedRoute = toMergeRoute;
        }

        return mergedRoute;
    }

    /**
     * Compares the cost of {@link RailroadStop} in a set and gets the least costly one.
     *
     * @param railroadStops a set of rail stops.
     * @return the least costly {@link RailroadStop}.
     */
    private RailroadStop getCheapestTrip(Set<RailroadStop> railroadStops) {
        return railroadStops.stream()
                .min(Comparator.comparing(RailroadStop::getCost))
                .orElseThrow(NoSuchRouteException::new);
    }

    /**
     * Updates the trip cost of a stop {@link RailroadStop} in case its previous cost is lower than
     * the new one.
     *
     * @param origin the origin stop
     * @param destination the destination stop
     * @param destinationCost the cost of going from the origin to the destination.
     */
    private void updateTripCost(RailroadStop origin, RailroadStop destination, long destinationCost) {

        //we assume that the origin stop is connected to the destination through a track
        long tentativeCost = origin.getCost() + destinationCost;

        if (tentativeCost < destination.getCost()) {

            destination.setCost(tentativeCost);
            destination.setPreviousStop(origin);

        }
    }

    /**
     * Creates a route {@link ShortestRoute} from a final stop.
     *
     * @param railroadStop the final stop from which we traverse its stops.
     * @return a route {@link ShortestRoute} made up by traversing stops.
     */
    private ShortestRoute traverseStops(RailroadStop railroadStop) {

        ShortestRoute route = new ShortestRoute();

        Stack<RailroadStop> stops = route.getStops();

        while (railroadStop.getPreviousStop() != null) {

            stops.push(railroadStop);

            railroadStop = railroadStop.getPreviousStop();

        }

        stops.push(railroadStop);

        return route;
    }
}