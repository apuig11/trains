package com.apuig.trains.core.service;

import com.apuig.trains.core.exception.NoSuchRouteException;
import com.apuig.trains.core.model.RailroadGraph;
import com.apuig.trains.core.model.RailroadStop;
import com.apuig.trains.core.model.Route;
import com.apuig.trains.core.model.ShortestRoute;

/**
 * {@code RailroadRouteService} defines an API for operating with rail routes {@link Route}.
 *
 *
 * @author Alejandro Puig Escobar
 */
public interface RailroadRouteService {

    /**
     * Gets the shortest route of a given graph {@link RailroadGraph} for several nodes.
     *
     * @param railroadGraph the graph in question.
     * @param stopNames stops to calculate.
     * @return the shortest route {@link ShortestRoute} calculated with the stops passed as arguments.
     */
    Route getRoute(RailroadGraph railroadGraph, String stopNames) throws NoSuchRouteException;
}
