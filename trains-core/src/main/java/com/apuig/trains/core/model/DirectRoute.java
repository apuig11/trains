package com.apuig.trains.core.model;

import com.apuig.trains.core.util.RouteUtil;

import java.util.LinkedList;
import java.util.List;

public class DirectRoute extends Route {

    private List<RailroadStop> stops = new LinkedList<>();

    @Override
    public List<RailroadStop> getStops() {
        return this.stops;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();

        RailroadStop[] stopsArr = this.stops.toArray(new RailroadStop[]{});

        stringBuilder.append("Direct route is as follows:");
        stringBuilder.append(System.lineSeparator());

        long cost = 0;

        for (int i = 0; i < stopsArr.length - 1; i++) {

            RailroadStop origin = stopsArr[i], destination = stopsArr[i + 1];

            cost += RouteUtil.getDirectStopDistance(origin, destination);

            this.setStopInfo(origin, destination, cost, stringBuilder);

        }

        return stringBuilder.toString();
    }
}
