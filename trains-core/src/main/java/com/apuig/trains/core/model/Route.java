package com.apuig.trains.core.model;

import com.apuig.trains.core.util.RouteUtil;

import java.util.Collection;

public abstract class Route {

    public abstract Collection getStops();

    public long getTotalDistance() {
        Collection stops = this.getStops();

        RailroadStop[] stopsArr = (RailroadStop[]) stops.toArray(new RailroadStop[]{});

        long cost = 0;

        for (int i = 0; i < stopsArr.length - 1 ; i++) {

            cost += RouteUtil.getDirectStopDistance(stopsArr[i], stopsArr[i + 1]);

        }

        return cost;

    }

    void setStopInfo(RailroadStop origin, RailroadStop destination, long cost, StringBuilder stringBuilder) {
        stringBuilder.append("From ");
        stringBuilder.append(origin.getName());
        stringBuilder.append(" to ");
        stringBuilder.append(destination.getName());
        stringBuilder.append(" with accumulated cost ");
        stringBuilder.append(cost);
        stringBuilder.append(System.lineSeparator());
    }


}
