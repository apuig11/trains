package com.apuig.trains.core.exception;

import com.apuig.trains.core.service.RailroadRouteService;

/**
 * Exception thrown in run-time {@link RuntimeException} when no route is found during
 * shortest route calculation performed by {@link RailroadRouteService}.
 *
 * @author Alejandro Puig Escobar
 */
public class NoSuchRouteException extends RuntimeException {

    private static final String NO_SUCH_ROUTE_MSG = "NO SUCH ROUTE";

    public NoSuchRouteException() {
        super(NO_SUCH_ROUTE_MSG);
    }
}
