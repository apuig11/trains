package com.apuig.trains.core.model;

import com.apuig.trains.core.service.RailroadRouteService;

import java.util.HashSet;
import java.util.Set;

/**
 * This class represents a set of different rail stops.
 * Can be understood as a graph used by {@link RailroadRouteService} to
 * compute the shortest path among all possibles.
 *
 * @author Alejandro Puig Escobar
 */
public class RailroadGraph {

    private Set<RailroadStop> railroadStops = new HashSet<>();

    public Set<RailroadStop> getRailroadStops() {
        return railroadStops;
    }

    public void addTownStop(RailroadStop stop) {
        this.railroadStops.add(stop);
    }

    @Override
    public String toString() {

        StringBuilder stringBuilder = new StringBuilder();

        this.getRailroadStops().stream().forEach(stop -> stringBuilder.append(stop.toString()));

        return stringBuilder.toString();
    }
}
