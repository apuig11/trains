package com.apuig.trains.core.exception;

/**
 * Exception thrown in run-time when creating graph instances with duplicated stop
 * connections.
 *
 * @author Alejandro Puig Escobar
 */
public class RouteAlreadyExistingException extends RuntimeException {
    private static final String ROUTE_ALREADY_EXISTS_MSG = "ROUTE ALREADY EXISTS";

    public RouteAlreadyExistingException() {
        super(ROUTE_ALREADY_EXISTS_MSG);
    }
}
