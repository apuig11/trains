package com.apuig.trains.core.exception;

import com.apuig.trains.core.model.RailroadGraph;

/**
 * Exception thrown in run-time {@link RuntimeException} when no stop is found in a rail map
 * {@link RailroadGraph}
 *
 * @author Alejandro Puig Escobar
 */
public class NoSuchStopException extends RuntimeException {
    private static final String NO_SUCH_STOP_MSG = "NO SUCH STOP";

    public NoSuchStopException() {
        super(NO_SUCH_STOP_MSG);
    }
}
