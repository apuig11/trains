# Trains Exercise

This exercise solves the problem of structuring and finding the most direct and the shortest route (respecitvely) of a given railroad graph.

### Prerequisites

* Java JDK 8+
* Maven 3+

## Build

In order to run the project build:
 
* Navigate to parent project directory:
 
```
 cd trains
```
 
* Execute the following command:

```
mvn package
```

The resulting package will be found at the relative path `target/trains-launcher-${project.version}.jar`

## Running Tests

The project consists of several unit tests with the purpose of testing the consistency of routes
 calculation either for SPF algorithm or direct route and also the creation of a graph node given user input.
 
*In order to run all the tests execute: 

```
mvn test
```

*Compiling the code will also run the tests by default.

## Running Code

* In order to run the code navigate to trains-launcher target folder:

```
cd trains/trains-launcher/target
```

* Execute the following command:

```
java -jar trains-launcher-${project.version}.jar
```

## Built With

* [Maven](https://maven.apache.org/) - Dependency Management
