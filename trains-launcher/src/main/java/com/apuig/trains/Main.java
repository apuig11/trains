package com.apuig.trains;

import com.apuig.trains.launcher.Launcher;
import com.apuig.trains.launcher.impl.TrainsLauncher;

/**
 * Contains main method.
 *
 * @author Alejandro Puig Escobar
 */
public class Main {

    public static void main(String[] args) {

        Launcher trainsLauncher = new TrainsLauncher();
        trainsLauncher.run();
    }

}
