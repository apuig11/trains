package com.apuig.trains.launcher.impl;

import com.apuig.trains.core.exception.NoSuchRouteException;
import com.apuig.trains.core.exception.NoSuchStopException;
import com.apuig.trains.core.exception.RouteAlreadyExistingException;
import com.apuig.trains.core.factory.RailroadFactory;
import com.apuig.trains.core.model.RailroadGraph;
import com.apuig.trains.core.model.Route;
import com.apuig.trains.core.service.RailroadRouteService;
import com.apuig.trains.core.service.impl.DirectRouteServiceImpl;
import com.apuig.trains.core.service.impl.ShortestRouteServiceImpl;
import com.apuig.trains.launcher.Launcher;

import java.util.Scanner;

/**
 * Implementation of {@link Launcher}.
 * Contains console logic for the trains exercise.
 *
 * @author Alejandro Puig Escobar
 */
public class TrainsLauncher implements Launcher {
    
    private static final Scanner SCANNER = new Scanner(System.in);

    @Override
    public void run() {

        System.out.println("Starting Trains Launcher..." + System.lineSeparator());

        RailroadRouteService shortestRouteService = ShortestRouteServiceImpl.getInstance();
        RailroadRouteService directRouteService = DirectRouteServiceImpl.getInstance();

        RailroadGraph railroadGraph = null;
        Route route;

        while (true) {

            System.out.println(Command.MANUAL);

            try {

                switch (Command.valueOf(Integer.valueOf(SCANNER.nextLine()))) {

                    case CREATE:
                        System.out.println("Creating railroad graph of:");
                        railroadGraph = RailroadFactory.getInstance(SCANNER.nextLine());
                        System.out.println(System.lineSeparator());
                        break;

                    case INFO:
                        System.out.println("Displaying current graph information:" + System.lineSeparator());
                        System.out.println(railroadGraph.toString() + System.lineSeparator());
                        break;

                    case CALCULATE_SHORTEST:
                        System.out.println("Calculating shortest route for:");
                        route = this.calculateRoute(shortestRouteService, railroadGraph, SCANNER.nextLine());
                        System.out.println(route.toString());
                        break;

                    case CALCULATE_DIRECT:
                        System.out.println("Calculating direct route for:");
                        route = this.calculateRoute(directRouteService, railroadGraph, SCANNER.nextLine());
                        System.out.println(route.toString());
                        break;

                    case EXIT:
                        SCANNER.close();
                        System.out.println("Exiting the program.");
                        return;

                    default:
                        throw new NumberFormatException();
                }

            } catch (NoSuchStopException e) {

                System.out.println("It has been indicated one stop that doesn't exist in the graph." +
                        System.lineSeparator());

            } catch (NoSuchRouteException e) {

                System.out.println("Can't find a route for such input." + System.lineSeparator());

            } catch (RouteAlreadyExistingException e) {

                System.out.println("It has been indicated a route more than once." + System.lineSeparator());

            } catch (NullPointerException e) {

                System.out.println("Railroad graph hasn't yet been initialized." + System.lineSeparator());

            } catch (NumberFormatException e) {

                System.out.println("Please introduce a valid command." + System.lineSeparator());

            } catch (IllegalArgumentException e) {

                System.out.println("Please introduce a valid input." + System.lineSeparator());

            }
        }
    }
    
    private Route calculateRoute(RailroadRouteService routeService, RailroadGraph railroadGraph, String route) {

        return routeService.getRoute(railroadGraph, route);

    }

    private enum Command {
        CREATE, INFO, CALCULATE_SHORTEST, CALCULATE_DIRECT, EXIT, NONE;

        private static final String MANUAL = "1. Create railroad graph by specifying the nodes and edges as input and" +
                " separated by ','. I.e. \"AB5,BC3,DC7\"." + System.lineSeparator() + "2. Get current railroad " +
                "graph information." + System.lineSeparator() + "3. Calculate shortest railroad route from one rail " +
                "to other/s. I.e. \"A-E-D-B\"." + System.lineSeparator() + "4. Calculate direct railroad route " +
                "from one rail to other/s. I.e. \"A-E-D-B\"." + System.lineSeparator() + "5. Exit." +
                System.lineSeparator();

        public static Command valueOf(int i) {
            switch (i) {
                case 1:
                    return CREATE;

                case 2:
                    return INFO;

                case 3:
                    return CALCULATE_SHORTEST;

                case 4:
                    return CALCULATE_DIRECT;

                case 5:
                    return EXIT;

                default:
                    return NONE;
            }
        }
    }
}
