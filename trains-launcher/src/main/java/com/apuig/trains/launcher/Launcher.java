package com.apuig.trains.launcher;

/**
 *
 * Interface with the purpose of decoupling application logic from main method.
 *
 * @author Alejandro Puig Escobar
 */
public interface Launcher {

    /**
     * Method to start up the launcher.
     */
    void run();

}
